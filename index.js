const CROSS = 'X';
const ZERO = 'O';
const EMPTY = ' ';
let fieldSize = Number(prompt('Задайте размер поля.'));
let randomizer = confirm('Хотите сыграть с компьютером?');
let crossTurn = true;
let winnerDetermined = false;

startGame();

function generateFieldArray(fieldSize) {
	let fieldArray = [];
	for (row = 0; row < fieldSize; row++) {
		fieldArray.push([])
		for (col = 0; col < fieldSize; col++) {
			fieldArray[row].push(-1);
		}
	}
	return fieldArray;
}

function startGame () {
	renderGrid(fieldSize);
	fieldArray = generateFieldArray(fieldSize);
	winnerDetermined = false;
	crossTurn = true;
	showMessage('');
}

function cellClickHandler (row, col) {
	if (fieldArray[row][col] === -1 && winnerDetermined === false) {
		if (randomizer === true) {
			makeTurnComputerVsHuman(row, col)
		} else {
			makeTurnHumanVsHuman(row, col)
		}
	}
}

function makeTurnComputerVsHuman(row, col) {
	renderSymbolInCell(CROSS, row, col);
	fieldArray[row][col] = 1;
	crossTurn = false;
	findWinnerOrDraw(fieldArray);
	if (winnerDetermined === false) {
		let randomNumber = generateNumber(fieldSize);
		while (true) {
			if (fieldArray[Math.floor(randomNumber / fieldSize)][randomNumber % fieldSize] !== -1) {
				randomNumber++;
			} else {
				break;
			}
			if (randomNumber > fieldSize - 1) {
				randomNumber = generateNumber(fieldSize);
			}
		}
		renderSymbolInCell(ZERO, Math.floor(randomNumber / fieldSize), randomNumber % fieldSize);
		fieldArray[Math.floor(randomNumber / fieldSize)][randomNumber % fieldSize] = 0;
		crossTurn = true;
		findWinnerOrDraw(fieldArray);
	}
}

function makeTurnHumanVsHuman(row, col) {
	if (crossTurn === true) {
		renderSymbolInCell(CROSS, row, col);
		fieldArray[row][col] = 1;
		crossTurn = false;
		findWinnerOrDraw(fieldArray);
	} else {
		renderSymbolInCell(ZERO, row, col);
		fieldArray[row][col] = 0;
		crossTurn = true;
		findWinnerOrDraw(fieldArray);
	}
}

function findWinnerOrDraw(fieldArray) {
	let combinationIndexesArray;
	let winnerSign;
	if (getRowWinnerCombination(fieldArray) !== -1) {
		combinationIndexesArray = getRowWinnerCombination(fieldArray);
		winnerDetermined = true;
	}
	if (getColWinnerCombination(fieldArray) !== -1) {
		combinationIndexesArray = getColWinnerCombination(fieldArray);
		winnerDetermined = true;
	}
	if (getMainDiagonalWinnerCombination(fieldArray) !== -1) {
		combinationIndexesArray = getMainDiagonalWinnerCombination(fieldArray);
		winnerDetermined = true;
	}
	if (getSecondaryDiagonalWinnerCombination(fieldArray) !== -1) {
		combinationIndexesArray = getSecondaryDiagonalWinnerCombination(fieldArray);
		winnerDetermined = true;
	}
	if (winnerDetermined === true) {
		winnerSign = getWinnerSign(combinationIndexesArray);
		highlightWinner(combinationIndexesArray, fieldArray, winnerSign);
	}
	if (winnerSign !== undefined) {
		if (winnerSign === CROSS) {
			showMessage('Победил пользователь с крестиками.');
		} else {
			showMessage('Победил пользователь с ноликами.');
		}
	}
	if (isDraw(fieldArray) && winnerDetermined === false) {
		showMessage('Победила дружба.');
		winnerDetermined = true;
	}
}

function getRowWinnerCombination(fieldArray) {
	let sumInRow;
	let emptyCellInRow;
	for (let row = 0; row < fieldSize; row++) {
		sumInRow = 0;
		emptyCellInRow = false;
		for (let col = 0; col < fieldSize; col++) {
			if (fieldArray[row][col] === -1) {
				emptyCellInRow = true;
				break;
			}
			sumInRow += fieldArray[row][col];
		}
		if (emptyCellInRow === false && (sumInRow === 0 || sumInRow === fieldSize)) {
			let combinationIndexesArray = [];
			for (let col = 0; col < fieldSize; col++) {
				combinationIndexesArray.push([row, col]);
			}
			return combinationIndexesArray;
		}
	}
	return -1;
}

function getColWinnerCombination(fieldArray) {
	let sumInCol;
	let emptyCellInCol;
	for (let col = 0; col < fieldSize; col++) {
		sumInCol = 0;
		emptyCellInCol = false;
		for (let row = 0; row < fieldSize; row++) {
			if (fieldArray[row][col] === -1) {
				emptyCellInCol = true;
				break;
			}
			sumInCol += fieldArray[row][col];
		}
		if (emptyCellInCol === false && (sumInCol === 0 || sumInCol === fieldSize)) {
			let combinationIndexesArray = [];
			for (let row = 0; row < fieldSize; row++) {
				combinationIndexesArray.push([row, col]);
			}
			return combinationIndexesArray;
		}
	}
	return -1;
}

function getMainDiagonalWinnerCombination(fieldArray) {
	let sumInMainDiagonal = 0;
	for (let i = 0; i < fieldSize; i++) {
		if (fieldArray[i][i] === -1) {
			return -1;
		} else {
			sumInMainDiagonal += fieldArray[i][i];
		}
	}
	if (sumInMainDiagonal === 0 || sumInMainDiagonal === fieldSize) {
		let combinationIndexesArray = [];
		for (let i = 0; i < fieldSize; i++) {
			combinationIndexesArray.push([i, i]);
		}
		return combinationIndexesArray;
	}
	return -1;
}

function getSecondaryDiagonalWinnerCombination(fieldArray) {
	let sumInSecondaryDiagonal = 0;
	for (let i = fieldSize-1; i >= 0; i--) {
		if (fieldArray[i][fieldSize-i-1] === -1) {
			return -1;
		} else {
			sumInSecondaryDiagonal += fieldArray[i][fieldSize-i-1];
		}
	}
	if (sumInSecondaryDiagonal === 0 || sumInSecondaryDiagonal === fieldSize) {
		let combinationIndexesArray = [];
		for (let i = fieldSize-1; i >= 0; i--) {
			combinationIndexesArray.push([i, fieldSize-i-1]);
		}
		return combinationIndexesArray;
	}
	return -1;
}

function isDraw(fieldArray) {
	for (let row = 0; row < fieldSize; row++) {
		for (let col = 0; col < fieldSize; col++) {
			if (fieldArray[row][col] === -1) {
				return 0;
			}
		}
	}
	return 1;
}

function getWinnerSign(combinationIndexesArray) {
	let row = combinationIndexesArray[0][0];
	let col = combinationIndexesArray[0][1];
	return fieldArray[row][col] === 1 ? CROSS : ZERO;
}

function highlightWinner(combinationIndexesArray, fieldArray, winnerSign) {
	let row;
	let col;
	for (let i = 0; i < combinationIndexesArray.length; i++) {
		row = combinationIndexesArray[i][0];
		col = combinationIndexesArray[i][1];
		renderSymbolInCell(winnerSign, row, col, color = '#ee262f');
	}
}

function generateNumber(fieldSize) {
	let randomNumber = Math.round(Math.random()*fieldSize**2);
	if (randomNumber === fieldSize**2) {
		return randomNumber - 1;
	} else {
		return randomNumber;
	}
}

/* обработчик нажатия на кнопку "Сначала" */
function resetClickHandler () {
	startGame();
}

/* Служебные фукнции для взаимодействия с DOM. Данные функции нельзя редактировать! */
/* Показать сообщение */
function showMessage(text) {
	var msg = document.querySelector('.message');
	msg.innerText = text
}

/* Нарисовать игровое поле заданного размера */
function renderGrid (dimension) {
	var container = getContainer();
	container.innerHTML = '';

	for (let i = 0; i < dimension; i++) {
		var row = document.createElement('tr');
		for (let j = 0; j < dimension; j++) {
			var cell = document.createElement('td');
			cell.textContent = EMPTY;
			cell.addEventListener('click', () => cellClickHandler(i, j));
			row.appendChild(cell);
		}
		container.appendChild(row);
	}
}

/* Нарисовать символ symbol в ячейку(row, col) с цветом color */
function renderSymbolInCell (symbol, row, col, color = '#333') {
	var targetCell = findCell(row, col);

	targetCell.textContent = symbol;
	targetCell.style.color = color;
}

function findCell (row, col) {
	var container = getContainer();
	var targetRow = container.querySelectorAll('tr')[row];
	return targetRow.querySelectorAll('td')[col];
}

function getContainer() {
	return document.getElementById('fieldWrapper');
}

/* Test Function */
/* Победа первого игрока */
function testWin () {
	clickOnCell(0, 2);
	clickOnCell(0, 0);
	clickOnCell(2, 0);
	clickOnCell(1, 1);
	clickOnCell(2, 2);
	clickOnCell(1, 2);
	clickOnCell(2, 1);
}

/* Ничья */
function testDraw () {
	clickOnCell(2, 0);
	clickOnCell(1, 0);
	clickOnCell(1, 1);
	clickOnCell(0, 0);
	clickOnCell(1, 2);
	clickOnCell(1, 2);
	clickOnCell(0, 2);
	clickOnCell(0, 1);
	clickOnCell(2, 1);
	clickOnCell(2, 2);
}

function clickOnCell (row, col) {
	findCell(row, col).click();
}
